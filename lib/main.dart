import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      AudioCache().play('note1.wav');
                    },
                    //fucking MaterialColors can not be extracted to method parameter
                    //the only one case is make background color without gradient
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFFB71C1C),
                            Color(0xFFE53935),
                            Color(0xFFEF9A9A),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    ),
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      AudioCache().play('note2.wav');
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFFE65100),
                            Color(0xFFFB8C00),
                            Color(0xFFFFCC80),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    ),
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      AudioCache().play('note3.wav');
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFFF57F17),
                            Color(0xFFFDD835),
                            Color(0xFFFFF59D),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    ),
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      AudioCache().play('note4.wav');
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFF1B5E20),
                            Color(0xFF43A047),
                            Color(0xFFA5D6A7),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    ),
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      AudioCache().play('note5.wav');
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFF42A5F5),
                            Color(0xFF64B5F6),
                            Color(0xFF90CAF9),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    ),
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      AudioCache().play('note6.wav');
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFF0D47A1),
                            Color(0xFF1565C0),
                            Color(0xFF1976D2),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    ),
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      AudioCache().play('note7.wav');
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Colors.deepPurple,
                            Colors.purple,
                            Colors.purpleAccent
//                            darkColorValue: 0xFF4A148C,
                            //        midColorValue: 0xFF8E24AA,
                            //        lightColorValue: 0xFFCE93D8,
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
